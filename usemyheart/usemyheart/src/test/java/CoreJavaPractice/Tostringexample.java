package CoreJavaPractice;

public class Tostringexample {
	
	int empid;
	String empname;
	float empsalary;
	
	Tostringexample(int empid,String empname, float empsalary ){
	this.empid= empid;
	this.empname =empname;
	this.empsalary=empsalary;
	}
	
//overiding Tostring() so that content of reference variable will print, If we will not overide it then classname@hashcode will print
	
	public String toString()
	
	{ return "emp id =" + empid + "emp name =" + empname + "emp salary =" + empsalary;
	}
	

	public static void main(String[] args) {
	
	Tostringexample obje = new Tostringexample(123, "rohit", 50000);
	System.out.println(obje);
	}

}
