package CoreJavaPractice;

import java.util.ArrayList;
import java.util.Collections;

public class Sorting {

public static void main(String[] args) {
	
Emp e1 = new Emp(123,"Ajay");
Emp e2 = new Emp(24,"Boby");
Emp e3 = new Emp(999,"diksha");
Emp e4 = new Emp(725,"eisha");

ArrayList<Emp> al = new ArrayList<Emp>();
al.add(e1);
al.add(e2);
al.add(e3);
al.add(e4);
Collections.sort(al);

for(Emp e:al){
System.out.println(e.eid+"-------"+e.ename);	
}
	
}

}
