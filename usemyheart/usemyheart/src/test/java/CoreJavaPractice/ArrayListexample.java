package CoreJavaPractice;

import java.util.ArrayList;

public class ArrayListexample {

public static void main(String[] args) {
	
/*Emp e = new Emp(2229, "Ajay");
Student s = new Student(11, "sahchit");*/
//Example 1
/*ArrayList al = new ArrayList();
al.add(e);
al.add(s);
al.add(10);
al.add("rar");

for(Object oo : al)
{
if (oo instanceof Emp)
{
Emp ee =  (Emp) oo;
System.out.println("employee id = " +"-----" + ee.eid + "employee ename = " +ee.ename);
}
}

System.out.println(al);*/

//Difference between Array and Array List
// Array is type safe, it means we are sure which data will be fetched on ther otherhand arrayList is not type safe becuase we are not sure which data will come
//so we must have to use generic
//Array support homogenous data type for e.g Integer array support Integer data on the other hand ArrayList support heterogenous data no primitive data will allowed.


Emp e1 = new Emp(2229, "Ajay");
Emp e2 = new Emp(229, "Ajay1");
Emp e3 = new Emp(400, "Ajay2");

ArrayList<Emp> al = new ArrayList<Emp>();
al.add(e1);
al.add(e2);
al.add(e3);

for (Emp e : al)	
	
System.out.print("emp id = " +e.eid + "--------" + " emp name = " +e.ename);
{
	
}
	
	
}

}
