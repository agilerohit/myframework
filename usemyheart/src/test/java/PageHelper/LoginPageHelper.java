package PageHelper;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.testng.Assert;
import org.testng.Reporter;
import Utilityfiles.ExcelUtils;
import Utilityfiles.DriverHelper;
import io.appium.java_client.AppiumDriver;

public class LoginPageHelper extends DriverHelper{
	
	String server = System.getProperty("server");

	public LoginPageHelper(AppiumDriver wd) {
		super(wd);
		// TODO Auto-generated constructor stub
	}
	
	
	//locator for login page
	
	String usernamepath ="com.upwork.android:id/email";
	String passwordpath = "com.upwork.android:id/password";
	String forgotpassword ="com.upwork.android:id/forgot_password";
	String register ="com.upwork.android:id/register";
	String loginbutton = "com.upwork.android:id/login";
	String alertMessageOkButtonPath = "android:id/button1";
	String alertMessageTitlePath= "android:id/alertTitle";
	String alertMessagePath="android:id/message";
	
	//Verify login page method
	
	public void verifyloginpage(){
	
		Assert.assertTrue(isElementPresent(usernamepath));								            Reporter.log("User Name is found", true);
		Assert.assertTrue(isElementPresent(passwordpath)); 							                Reporter.log("Password is found", true);
		Assert.assertTrue(isElementPresent(loginbutton));							                Reporter.log("login Button is found", true);
		}
	
	public void enterCredentials(String userName, String password) {
		String username = "";
		if (server.contains("stage")) {
			username = "stage:" + userName;
		} else {
			username = userName;
		}
		clickOn(usernamepath);
		System.out.println("UserName: " + username);
		sendKeys(usernamepath, username);
		clickOn(passwordpath);
		sendKeys(passwordpath, password);
		//clickOn(loginbutton);
	}
	
	public void clickonloginbutton()
	{
		clickOn(loginbutton);
	}
	
	// Click on the What' New alert to dismiss the alert
    public void ClickonMessageAlert() {
   
    clickOn(alertMessageOkButtonPath);
   }
	
	
 //Pass parameter from excel file
	
public void enterintoApplication() throws Exception{
		
		
		sendKeys(usernamepath, ExcelUtils.getCellData(1, 2));
		clickOn(passwordpath);
		sendKeys(passwordpath,  ExcelUtils.getCellData(1, 3));
		clickOn(loginbutton);
	}
	

	
	String username1;
	String password1;
	
	public void enterCredentials(){
		
		{
			try
			
			{
				File file1 = new File ("D:\\usemyheart\\src\\test\\java\\Data\\testdata.xls");
				System.out.println(file1.getAbsolutePath());
				FileInputStream fn1 = new FileInputStream(file1);
				System.out.println(fn1.hashCode());
				HSSFWorkbook workbook = new HSSFWorkbook(fn1);
				HSSFSheet sheet = workbook.getSheetAt(0);
				for (int count=1; count<=sheet.getLastRowNum(); count++){
					
					HSSFRow row = sheet.getRow(count);
					username1 = row.getCell(1).toString();
					 
					System.out.println(username1);
					password1 = row.getCell(2).toString();
					System.out.println(password1);
					 
				}
				
			}
			
			catch (Exception e)
			
			{
				
				e.printStackTrace();
			}}
			
		}
		
		
		/*sendKeys(username, username);
		clickOn(password);
		sendKeys(password, password);
		clickOn(loginbutton);
	}*/
	
public String startTime()
    
    {
    	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String stime = dateFormat.format(cal.getTime());
		return stime;	 
    	 }
    
    public String endTime(){
    	
    	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String etime = dateFormat.format(cal.getTime());
		return etime;
    }
    
    public long totalTime() throws ParseException{
    	
    	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm:ss");
		String stime = dateFormat.format(cal.getTime());
		System.out.println(stime);
		String etime = dateFormat.format(cal.getTime());
		System.out.println(etime);
		java.util.Date date1 = df.parse(stime);
        java.util.Date date2 = df.parse(etime);
        long ttime = date2.getTime() - date1.getTime();
        System.out.println(ttime);
		return ttime;
		
		

		
		
		
		

        
    	
		
    	
    } 


}