package Utilityfiles;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public abstract class DriverHelper {
	
protected static AppiumDriver driver;

public DriverHelper(AppiumDriver wd){
driver = wd;
}
public static void WaitForElementPresent(String locator, int timeout)  {
WebDriverWait wait = new WebDriverWait(driver, timeout);
wait.until(ExpectedConditions.visibilityOfElementLocated(byLocator(locator)));
}

/*Function check element present or not
 *Return type Boolean
 */
public static Boolean isElementPresent(String locator){
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
Boolean result = false;
try{
 driver.findElement(byLocator(locator));
 result = true;
}catch(Exception e){
        //e.printStackTrace();
    }
return result;
}

// Handle locator type
public static  By byLocator(final String locator) {
    By result = null;
    
    if (locator.startsWith("//")) {
        result = By.xpath(locator);
    } else if (locator.startsWith("css=")) {
        result = By.cssSelector(locator.replace("css=", ""));
    } else if (locator.startsWith("#")) {
        result = By.name(locator.replace("#", ""));
    } else if (locator.startsWith("xpath=")) {
        result = By.xpath(locator);
    } else {
        result = By.id(locator);
    }
    return result;
}

public void clickOn(String locator){
WaitForElementPresent(locator, 10);
driver.findElement(byLocator(locator)).click();
}

public void sendKeys(String locator, String enterText){
WaitForElementPresent(locator, 10);
driver.findElement(byLocator(locator)).clear();      
driver.findElement(byLocator(locator)).sendKeys(enterText);       
}


public void clearText(String locator){
WaitForElementPresent(locator, 10);
driver.findElement(byLocator(locator)).clear();
}

public String getText(String locator){
String text = driver.findElement(byLocator(locator)).getText();
return text;
}

public int getXpathCount(String locator){
int count = driver.findElements(byLocator(locator)).size();
return count;
}

public void check(String locString){
System.out.println("Displayed: "+driver.findElement(byLocator(locString)).isDisplayed());
System.out.println("Enabled: "+driver.findElement(byLocator(locString)).isEnabled());
}

public Boolean isChecked(String locator){
    boolean checkedStatus = false;
    checkedStatus = driver.findElement(byLocator(locator)).isSelected();
    System.out.println("checkedStatus: "+checkedStatus);
    return checkedStatus;
}

public String getAttribute(String locator, String attribute){
    String text = driver.findElement(byLocator(locator)).getAttribute(attribute);
    return text;
}

public void acceptAlert()
{
    driver.switchTo().alert().accept();
}


// MoveToElement
public void moveToElement(String locator)
{
    Actions action = new Actions(driver);
    action.moveToElement(driver.findElement(byLocator(locator))).build().perform();
 
}

public Boolean scrollUntilElementFound(String locator)
{
    
    boolean flag = false;
    for (int i=0; i<=10 && flag == false; i++){
    	flag = isElementPresent(locator);
    	if (!flag) {
    		scrollDown();
    	}
    }
    return flag;
}


  // Scroll Down the window to specific text
    public void scrollDown()
    {
    	System.out.println("Scroll down...");
        try {
        	Runtime.getRuntime().exec("adb shell input keyevent 20");
        	} catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    
    /**
     * This function swipe the screen direction wise.
     * @param direction (Left, Right)
     */
    public void swipescreen(String direction) {
        int startX =0;
        int endX =0;
        int startY =0;
        int endY =0;
        
        driver.context("NATIVE_APP");
        org.openqa.selenium.Dimension size = driver.manage().window().getSize();
        if (direction.contains("left")) {
            endX = (int) (size.width * 0.10);
            startX = (int) (size.width * 0.80);
            startY = size.height / 2;
            endY = size.height / 2;
        }
        
        if (direction.contains("right")) {
            startX = (int) (size.width * 0.10);
            endX = (int) (size.width * 0.80);
            startY = size.height / 2;
            endY = size.height / 2;
        }
        
        driver.swipe(startX, startY, endX, endY, 1000);
    }
    
    
    
    /**
     * This function scroll the screen direction wise.
     * @param direction(Up, Down)
     */
    public void scrollScreen(String direction){
        int startX = 0;
        int endX = 0;
        int startY = 0;
        int endY = 0;
        
        driver.context("NATIVE_APP");
        org.openqa.selenium.Dimension size = driver.manage().window().getSize();
        if (direction.contains("down")) {
            
            startY = (int) (size.height * 0.30);
            endY = startY+130;
            startX = size.width / 2;
            endX = size.width / 2;
        }
        
        if (direction.contains("up")) {
            startY = (int) (size.height * 0.80);
            endY = startY-130;
            startX = size.width / 2;
            endX = size.width / 2;
        }
        
        driver.swipe(startX, startY, endX, endY, 1000);
        
    }
    
    
    public void genric_Swipe(String direction){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("direction", direction);
        js.executeScript("mobile: scroll", scrollObject);
    }
    public String createTimestamp(){
        
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String string  = dateFormat.format(now);
        System.out.println(string);
        
        return string;
    }
    
    public void clickOnNotification(String notification)
    {
        ((AndroidDriver) driver).openNotifications();
        driver.findElement(By.name(notification)).click();
    }
    
    public void clickOnHomeButton()
    {
        ((AndroidDriver) driver).openNotifications();
        try {
            Runtime.getRuntime().exec(new String[] {"adb", "shell", "input", "keyevent", "3"});
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static void keyPress() 
    {
  
        try {
              /*CharSequence seq="66";*/
               ((AndroidDriver)driver).pressKeyCode(AndroidKeyCode.ENTER);      
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }      
    }
    
    /**
     * Press Android phone with Android KeyEvent
     * @param int androidKeyEvent
     * @return void
     */
    public static void sendKeyEvent(int androidKeyCode){
    	try{
    		((AndroidDriver)driver).pressKeyCode(androidKeyCode);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    public int changeStringToInt(String strng){
        String str = strng.replaceAll(",", "");
        str = str.replace("$", "");
        str = str.replace("/hr", "");
        Float f1 = Float.parseFloat(str);
        int i =Math.round(f1);
        return i;
    }
    
    public void longPress(String locator)
    {
    	TouchAction action = new TouchAction(driver);
		action.longPress(driver.findElement(byLocator(locator))).release().perform();
    }
    
   
    public boolean isMarshmallow()
    {
    	String deviceAPI=driver.getCapabilities().getCapability("platformVersion").toString();
    	//System.out.println("Device Version "+deviceAPI);
    	if (!"6.0".equals(deviceAPI))
    		return false;
    	else
    		return true;
    }
    
    public boolean isEmulator()
    {
    	boolean result = false;
    	String deviceType=driver.getCapabilities().getCapability("udid").toString();
    	//System.out.println("Device Version "+deviceAPI);
    	if (deviceType != null){
        	if (deviceType.toLowerCase().contains("emulator")){
        		result = true;
        	}
    	}
    	return result;
    }

}
