package Utilityfiles;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;



public class ExcelUtils {
	
public static HSSFWorkbook workbook;
public static HSSFSheet sheet;
public static HSSFRow row;
public static HSSFCell cell;
int rowNum =1;

public static void setExcelFile ( ) throws Exception {
	
	try {
		FileInputStream Fin = new FileInputStream(Constant.Path_TestData);
		System.out.println(Fin.hashCode());
		workbook = new HSSFWorkbook(Fin);
		sheet = workbook.getSheetAt(0);
		}
	
	catch ( Exception e){
		
		throw (e);
	}
}
	
	public static String getCellData ( int RowNum,int ColNum) throws Exception {
		
		setExcelFile ( );
		try{
			System.out.print(1);
			cell = sheet.getRow(RowNum).getCell(ColNum);
			System.out.print(2);
			String CellData = cell.getStringCellValue();
			System.out.print(3);
			return CellData;
		}
		
		catch(Exception e)
		{
		throw (e);
		}
	}
	
	public static void setData (int rowRum,String status, String Stime,String Etime,long Ttime) throws Exception{
		
		try {
			
			HSSFRow row = sheet.getRow(rowRum);
			HSSFCell cell = row.createCell(4);
			HSSFCell cell1 = row.createCell(5);
			HSSFCell cell2 = row.createCell(6);
			HSSFCell cell3 = row.createCell(7);
			cell.setCellValue("Pass");
			cell1.setCellValue(Stime);
			cell2.setCellValue(Etime);
			cell3.setCellValue(Ttime);
			
		}
		
		catch(Exception e)
		{
		throw (e);
		}
	}
	
	
	
}