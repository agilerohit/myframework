package CoreJavaPractice;

public class Emp implements Comparable{

	int eid;
	String ename;
	Emp(int eid, String ename)
	{
	this.eid=eid;
	this.ename =ename;
	}
	
	//Sorting for id
	/*public int compareTo(Object o) {
	Emp e = (Emp)o;
	if (eid==e.eid)
	{
	return 0;	
	}
	else if (eid>e.eid)
	{
	return 1;	
	}
	
	else{
	return -1;
	}
	
	}*/
	
	//Sorting using name
	public int compareTo(Object o) {
	Emp e = (Emp)o;
	return ename.compareTo(e.ename);
	}

}
