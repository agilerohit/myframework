package CoreJavaPractice;
public class ArrayExample {
public static void main(String[] args) {
	

//Array used to represent group of entity, these elements must be homogenous in nature	
//one way of array format
/*int []a;
a =new int[5];
a[0]=1000;
a[1]=15000;
a[3]=16000;
a[4]=19000;*/

//second way of array format
/*int[] a= {10, 11,12,15,18,25};*/

//one way to print array
/*System.out.println(a[0]);*/

//other way to print array
/*for(int i=0; i<a.length ;i++)
{
System.out.println(a[i]);
}*/

//for each loop to print array
 /*for(int aa : a)
 {
 System.out.println(aa); 
 }*/
	
//Default value will print	
/*int[] a= new int[5];
for(int aa :a)
{
System.out.println(aa);
}*/
	
/*A[] a1 = new A[3];
for(A a2 :a1)
{
System.out.println(a2);	
}*/
	
Emp e1 = new Emp(111, "rohit");	
Emp e2 = new Emp(222, "rohit1");	
Emp e3 = new Emp(333, "rohit2");	

Emp[] e = new Emp[5];
e[0] = e1;
e[2] = e2;
e[4] = e3;

/*for (Emp ee : e)
{
System.out.println(ee.eid+"---"+ee.ename);
}
// Null pointer exception will print
*/
for (Object o : e)
{
if (o instanceof Emp)
{
Emp ee = (Emp) o;
System.out.println(ee.eid+"----"+ee.ename);
}
	
if ( o==null)
{
System.out.println("null");
}
}

//Interview question,I need to print index value where value is null
for(int i=0; i<e.length; i++)
if(e[i]==null){
System.out.println(i);	
}

//Example Array method and Array paramter



}



}
